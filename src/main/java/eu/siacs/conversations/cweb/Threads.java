package eu.siacs.conversations.cweb;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;

import com.google.common.base.Supplier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class Threads {
    private static class ExceptionCatchingScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {
        public ExceptionCatchingScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory) {
            super(corePoolSize, threadFactory);
        }

        public ExceptionCatchingScheduledThreadPoolExecutor(int corePoolSize) {
            super(corePoolSize);
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            if (t == null && r instanceof Future<?>) {
                try {
                    ((Future<?>) r).get();
                } catch (CancellationException | InterruptedException ce) {
                    t = ce;
                } catch (ExecutionException ee) {
                    t = ee.getCause();
                }
            }
            if (t != null) {
                Log.e("afterExecute", "", t);
            }
        }
    }

    private static final ScheduledExecutorService lowPriorityExecutorService =
            new ExceptionCatchingScheduledThreadPoolExecutor(1, new ThreadFactory() {
                @Override
                public Thread newThread(@NonNull Runnable r) {
                    Thread thread = new Thread(r);
                    thread.setPriority(Thread.NORM_PRIORITY - 1);
                    return thread;
                }
            });

    private static final ScheduledExecutorService highPriorityExecutorService =
            new ExceptionCatchingScheduledThreadPoolExecutor(1);

    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    public static boolean isUIThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static void submitBackgroundTask(Runnable task) {
        lowPriorityExecutorService.submit(task);
    }

    public static void submitBackgroundTask(Runnable task, long delay) {
        lowPriorityExecutorService.schedule(task, delay, TimeUnit.MILLISECONDS);
    }

    public static void submitHighPriorityTask(Runnable task) {
        highPriorityExecutorService.submit(task);
    }

    public static void runOnUI(Runnable task) {
        mainThreadHandler.post(task);
    }

    public static void runOnUI(Runnable task, long delay) {
        mainThreadHandler.postDelayed(task, delay);
    }

    public static Thread runAsNewThread(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
        return thread;
    }

    public static void join(Thread thread) {
        try {
            thread.join();
        } catch (InterruptedException e) {
        }
    }

    public static <T> T runAsNewThreadBlocking(Supplier<T> supplier) {
        final List<T> result = new ArrayList<>();
        Thread thread = runAsNewThread(() -> {
            result.add(supplier.get());
        });
        join(thread);
        return result.get(0);
    }
}
