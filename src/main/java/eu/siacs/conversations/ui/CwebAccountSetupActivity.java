package eu.siacs.conversations.ui;

import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.common.base.Preconditions;

import org.apache.commons.lang3.StringUtils;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.siacs.conversations.Config;
import eu.siacs.conversations.R;
import eu.siacs.conversations.cweb.CwebService;
import eu.siacs.conversations.cweb.Threads;
import eu.siacs.conversations.cweb.XmppAdapter;
import eu.siacs.conversations.databinding.ActivityCwebAccountSetupBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountEmbeddedStorageBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountIntroBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountInviteApplyBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountInviteRequestBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountInviteRespond1Binding;
import eu.siacs.conversations.databinding.FragmentCwebAccountInviteRespond2Binding;
import eu.siacs.conversations.databinding.FragmentCwebAccountSelfHostBinding;
import eu.siacs.conversations.databinding.FragmentCwebAccountUpdateBinding;
import eu.siacs.conversations.databinding.FragmentCwebHostedAccountsBinding;
import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.ui.util.AvatarWorkerTask;

public class CwebAccountSetupActivity extends XmppActivity {
    private static final List<Class<? extends AccountSetupFragment>> SELF_HOST_FRAGMENTS = Arrays.asList(SelfHostFragment.class);
    private static final List<Class<? extends AccountSetupFragment>> JOIN_FRAGMENTS = Arrays.asList(InviteRequestFragment.class, InviteApplyFragment.class);

    private ActivityCwebAccountSetupBinding binding;
    private SetupPagerAdapter setupPagerAdapter;
    private PrivateStorageProfile embeddedStorageProfile;
    private Account account;
    private String nickname;
    private int position;
    private Flow flow;
    private StorageProfileRequest inviteRequest;
    private String inviteResponseSubdir;

    enum Flow {
        UNSET,
        SELF_HOST,
        JOIN,
        INVITE_RESPOND,
        HOSTED_ACCOUNTS,
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cweb_account_setup);

        embeddedStorageProfile = CwebService.getInstance().getEmbeddedStorageProfile();
    }

    private void init() {
        position = 0;
        flow = Flow.UNSET;

        if (account != null) {
            nickname = account.getDisplayName();
        }

        setupPagerAdapter = new SetupPagerAdapter(getSupportFragmentManager());
        binding.accountSetupViewPager.addOnPageChangeListener(onPageChangeListener);
        binding.accountSetupViewPager.setAdapter(setupPagerAdapter);

        binding.nextButton.setOnClickListener(v -> next());
        binding.backButton.setOnClickListener(v -> back());
    }

    private final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int page) {
            position = page;
            AccountSetupFragment fragment = setupPagerAdapter.fragments.get(page);
            fragment.onSelected();
            CwebAccountSetupActivity.this.updateButtonStatus();
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }
    };

    private void updateButtonStatus() {
        binding.buttonBar.setVisibility(embeddedStorageProfile != null || account == null || position != 0 ? View.VISIBLE : View.GONE);
        int page = binding.accountSetupViewPager.getCurrentItem();
        AccountSetupFragment fragment = setupPagerAdapter.fragments.get(page);
        int nextButtonResource = fragment.overrideNextButtonName();
        binding.nextButton.setText(nextButtonResource != -1 ? nextButtonResource : R.string.next);
        int size = binding.accountSetupViewPager.getAdapter().getCount();
        binding.nextButton.setEnabled(embeddedStorageProfile != null || size > 1);
        binding.backButton.setEnabled(page > 0);
    }

    @Override
    public void onBackPressed() {
        if (binding.backButton.isEnabled()) {
            back();
        } else {
            if (account != null) {
                super.onBackPressed();
            }
        }
    }

    private void next() {
        // Handle some edge cases where adapter isn't initialized
        if (position >= setupPagerAdapter.fragments.size()) {
            Log.w(Config.LOGTAG, "position " + position + " out of range for in the adapter " + setupPagerAdapter.fragments.size());
            return;
        }
        AccountSetupFragment fragment = setupPagerAdapter.fragments.get(position);
        if (!fragment.onNext()) {
            return;
        }
        int size = binding.accountSetupViewPager.getAdapter().getCount();
        if (position + 1 >= size) {
            finish();
            return;
        }
        moveToPage(position + 1);
    }

    private void back() {
        moveToPage(position - 1);
    }

    private void moveToPage(int position) {
        binding.accountSetupViewPager.setCurrentItem(position);
    }

    @Override
    public void finish() {
        if (account == null) {
            Intent intent = new Intent(this, StartConversationActivity.class);
            StartConversationActivity.addInviteUri(intent, getIntent());
            startActivity(intent);
        }
        super.finish();
    }

    private void onFlowSelected(Flow flow) {
        if (this.flow == flow) {
            return;
        }
        this.flow = flow;
        setupPagerAdapter.resetFlow();
        binding.accountSetupViewPager.setAdapter(setupPagerAdapter);
    }

    private String setSelfHostedStorageProfile(String storageProfileStr) {
        PrivateStorageProfile newPrivateStorageProfile = CwebService.extractAccountStr(storageProfileStr);
        if (newPrivateStorageProfile == null) {
            return "Invalid storage profile";
        }
        return setStorageProfile(newPrivateStorageProfile, false, null);
    }

    private String setStorageProfile(PrivateStorageProfile newPrivateStorageProfile, boolean fromEmbedded, StorageProfileResponse invite) {
        if (account != null) {
            org.apache.commons.lang3.tuple.Pair<Boolean, String> updated = CwebService.getInstance().updatePrivateStorageProfile(newPrivateStorageProfile, invite);
            if (updated.getRight() != null) {
                return updated.getRight();
            }
        } else {
            Pair<Account, String> result = CwebService.getInstance().createCwebAccount(xmppConnectionService, nickname, newPrivateStorageProfile, fromEmbedded, invite);
            if (result.second != null) {
                return result.second;
            }
        }
        return null;
    }

    private void setNickname(String nickname) {
        account.setDisplayName(nickname);
        if (!xmppConnectionService.updateAccount(account)) {
            Toast.makeText(this, R.string.unable_to_update_account, Toast.LENGTH_SHORT).show();
            return;
        }
        CwebService.getInstance().updateNickname(nickname);
        this.nickname = nickname;
    }

    @Override
    public void refreshUiReal() {
    }

    @Override
    void onBackendConnected() {
        account = XmppAdapter.findCwebAccount(xmppConnectionService);
        if (flow == null) {
            init();
        }
    }

    private boolean isBackgroundRestricted() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P && activityManager != null) {
            return activityManager.isBackgroundRestricted();
        }
        return false;
    }

    static abstract class AccountSetupFragment extends Fragment {
        abstract void onSelected();
        abstract boolean onNext();
        int overrideNextButtonName() {
            return -1;
        }
    }

    public class SetupPagerAdapter extends FragmentStatePagerAdapter {
        private final List<AccountSetupFragment> fragments;

        SetupPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        @Override
        public int getItemPosition(Object object) {
            Class<?> klass = object.getClass();
            if (SELF_HOST_FRAGMENTS.contains(klass) && flow != Flow.SELF_HOST ||
                    JOIN_FRAGMENTS.contains(klass) && flow != Flow.JOIN) {
                return POSITION_NONE;
            }
            return POSITION_UNCHANGED;
        }

        void resetFlow() {
            if (fragments.size() > 1) {
                fragments.subList(1, fragments.size()).clear();
            }
        }

        @Override
        public int getCount() {
            switch (flow) {
                case UNSET:
                    return 1;
                case SELF_HOST:
                    return 2;
                case JOIN:
                    return 3;
                case INVITE_RESPOND:
                    return 3;
                case HOSTED_ACCOUNTS:
                    return 2;
                default:
                    return 0;
            }
        }

        @Override
        public Fragment getItem(int position) {
            if (position < fragments.size()) {
                return fragments.get(position);
            }
            AccountSetupFragment fragment = null;
            if (position == 0) {
                if (embeddedStorageProfile != null) {
                    fragment = new EmbeddedStorageFragment();
                } else if (account == null) {
                    fragment = new IntroFragment();
                } else {
                    fragment = new AccountUpdateFragment();
                }
            } else if (flow == Flow.SELF_HOST) {
                switch (position) {
                    case 1:
                        fragment = new SelfHostFragment();
                        break;
                    default:
                        Preconditions.checkArgument(false);
                }
            } else if (flow == Flow.JOIN) {
                switch (position) {
                    case 1:
                        fragment = new InviteRequestFragment();
                        break;
                    case 2:
                        fragment = new InviteApplyFragment();
                        break;
                    default:
                        Preconditions.checkArgument(false);
                }
            } else if (flow == Flow.INVITE_RESPOND) {
                switch (position) {
                    case 1:
                        fragment = new InviteRespond1Fragment();
                        break;
                    case 2:
                        fragment = new InviteRespond2Fragment();
                        break;
                    default:
                        Preconditions.checkArgument(false);
                }
            } else if (flow == Flow.HOSTED_ACCOUNTS) {
                switch (position) {
                    case 1:
                        fragment = new HostedAccountsFragment();
                        break;
                    default:
                        Preconditions.checkArgument(false);
                }
            }
            Preconditions.checkArgument(position == fragments.size());
            fragments.add(fragment);
            if (CwebAccountSetupActivity.this.position == 0) {
                onPageChangeListener.onPageSelected(0);
            }
            return fragment;
        }
    }

    public static class EmbeddedStorageFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountEmbeddedStorageBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity)requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_embedded_storage, container, false);

            AvatarWorkerTask.loadAvatar(accountSetupActivity.account, binding.accountImage, R.dimen.avatar);
            binding.accountImage.setOnClickListener(view -> {
                Intent intent = new Intent(accountSetupActivity.getApplicationContext(), PublishProfilePictureActivity.class);
                intent.putExtra(EXTRA_ACCOUNT, accountSetupActivity.account.getJid().asBareJid().toEscapedString());
                startActivity(intent);
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {
            if (accountSetupActivity.nickname != null) {
                binding.nickname.setText(accountSetupActivity.nickname);
            }
        }

        @Override
        boolean onNext() {
            String nickname = StringUtils.trim(binding.nickname.getText().toString());
            if (StringUtils.isBlank(nickname)) {
                binding.nickname.setError("Invalid nickname");
                binding.nickname.requestFocus();
                return false;
            }

            if (accountSetupActivity.account == null) {
                accountSetupActivity.nickname = nickname;
                String error = accountSetupActivity.setStorageProfile(accountSetupActivity.embeddedStorageProfile, true, null);
                if (error != null) {
                    Toast.makeText(accountSetupActivity, error, Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else {
                accountSetupActivity.setNickname(nickname);
            }

            return true;
        }

        @Override
        int overrideNextButtonName() {
            return R.string.done;
        }
    }

    public static class IntroFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountIntroBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity)requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_intro, container, false);

            binding.hostingSelector.setOnCheckedChangeListener((radioGroup, checkedId) -> {
                Flow flow = checkedId == binding.selfHostToggle.getId() ? Flow.SELF_HOST : Flow.JOIN;
                // The null condition happens on orientation change
                if (accountSetupActivity.setupPagerAdapter != null) {
                    accountSetupActivity.onFlowSelected(flow);
                }
            });

            binding.cwebRestoreBackup.setOnClickListener(view -> {
                if (accountSetupActivity.hasStoragePermission(WelcomeActivity.REQUEST_IMPORT_BACKUP)) {
                    startActivity(new Intent(accountSetupActivity, ImportBackupActivity.class));
                }
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            String nickname = StringUtils.trim(binding.nickname.getText().toString());
            if (StringUtils.isBlank(nickname)) {
                binding.nickname.setError("Invalid nickname");
                binding.nickname.requestFocus();
                return false;
            }
            accountSetupActivity.nickname = StringUtils.trim(nickname);
            CwebService.getInstance().loadOrCreateLocalIdentity();
            return true;
        }
    }

    public static class SelfHostFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountSelfHostBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity)requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_self_host, container, false);
            binding.selfHostInfo.setMovementMethod(LinkMovementMethod.getInstance());
            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            final String storageProfileStr = this.binding.newStorageProfile.getText().toString().replaceAll("[\r\n]", "").trim();
            String error = accountSetupActivity.setSelfHostedStorageProfile(storageProfileStr);
            if (error != null) {
                binding.newStorageProfile.setError("Invalid storage profile");
                binding.newStorageProfile.requestFocus();
                return false;
            }
            return true;
        }

        @Override
        int overrideNextButtonName() {
            return R.string.done;
        }
    }

    public static class InviteRequestFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountInviteRequestBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity) requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_invite_request, container, false);
            binding.inviteRequestInfo.setMovementMethod(LinkMovementMethod.getInstance());

            binding.inviteRequestText.setOnFocusChangeListener((view, focus) -> {
                if (!focus) {
                    return;
                }
                ClipboardManager clipboard = (ClipboardManager) accountSetupActivity.getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("StoneAge invite request", binding.inviteRequestText.getText());
                clipboard.setPrimaryClip(clip);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                    Toast.makeText(accountSetupActivity, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
                }
            });

            binding.inviteRequestShare.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "StoneAge invite request");
                intent.putExtra(Intent.EXTRA_TEXT, binding.inviteRequestText.getText().toString());
                this.startActivity(Intent.createChooser(intent, "Send request to..."));
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {
            binding.inviteRequestText.setText(CwebService.getInstance().getInviteRequestText(accountSetupActivity.nickname));
        }

        @Override
        boolean onNext() {
            return true;
        }
    }

    public static class InviteApplyFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountInviteApplyBinding binding;
        private StorageProfileResponse invite;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity) requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_invite_apply, container, false);

            binding.inviteApplyInput.addTextChangedListener(new TextWatcher() {
                @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void afterTextChanged(Editable editable) {
                    StorageProfileResponse response = CwebService.getInstance().parseInviteResponseText(editable.toString());
                    if (response == null) {
                        binding.inviteApplyInput.setError(StringUtils.isBlank(editable.toString()) ? null : "Invalid invite");
                        binding.inviteApplyStorageProfile.setText("");
                        invite = null;
                        return;
                    }
                    if (response.equals(invite)) {
                        return;
                    }
                    invite = response;

                    binding.inviteApplyInput.setError(null);
                    binding.inviteApplyAdminTitle.setVisibility(View.VISIBLE);
                    binding.inviteApplyAdminName.setText(CwebService.getInstance().getNickname(response.getAdmin()));
                    String cwebStorageProfileStr = StorageProfileUtils.toHumanReadableString(StorageProfileUtils.toPublicStorageProfile(response.getPrivateStorageProfile()));
                    binding.inviteApplyStorageProfileTitle.setVisibility(View.VISIBLE);
                    binding.inviteApplyStorageProfile.setText(cwebStorageProfileStr);
                }
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            if (invite == null) {
                binding.inviteApplyInput.setError("Invalid invite");
                binding.inviteApplyStorageProfile.requestFocus();
                return false;
            }
            String error = accountSetupActivity.setStorageProfile(invite.getPrivateStorageProfile(), false, invite);
            if (error != null) {
                binding.inviteApplyStorageProfile.setError("Invalid storage profile");
                binding.inviteApplyStorageProfile.requestFocus();
                return false;
            }
            return true;
        }

        @Override
        int overrideNextButtonName() {
            return R.string.done;
        }
    }

    public static class AccountUpdateFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountUpdateBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity)requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_update, container, false);

            AvatarWorkerTask.loadAvatar(accountSetupActivity.account, binding.accountImage, R.dimen.avatar);
            binding.accountImage.setOnClickListener(view -> {
                Intent intent = new Intent(accountSetupActivity.getApplicationContext(), PublishProfilePictureActivity.class);
                intent.putExtra(EXTRA_ACCOUNT, accountSetupActivity.account.getJid().asBareJid().toEscapedString());
                startActivity(intent);
            });

            binding.nickname.setText(accountSetupActivity.nickname);
            binding.nickname.setOnFocusChangeListener((view, focus) -> {
                if (focus) {
                    return;
                }
                final String nickname = binding.nickname.getText().toString().trim();

                if (StringUtils.isBlank(nickname)) {
                    binding.nickname.setError("Invalid nickname");
                    binding.nickname.requestFocus();
                    return;
                }
                if (nickname.equals(accountSetupActivity.account.getDisplayName())) {
                    return;
                }

                accountSetupActivity.setNickname(nickname);
            });

            binding.currentStorageProfile.setText(CwebService.getInstance().getPublicStorageProfileStr());
            binding.currentStorageProfile.setOnClickListener(view -> {
                TextView tv = (TextView) view;
                int collapsedMaxLines = 2;
                ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", tv.getMaxLines() == collapsedMaxLines ? tv.getLineCount() : collapsedMaxLines);
                animation.setDuration(200).start();
            });


            binding.selfHostButton.setOnClickListener(view -> {
                accountSetupActivity.onFlowSelected(Flow.SELF_HOST);
                accountSetupActivity.moveToPage(1);
            });

            binding.joinButton.setOnClickListener(view -> {
                accountSetupActivity.onFlowSelected(Flow.JOIN);
                accountSetupActivity.moveToPage(1);
            });

            if (!CwebService.getInstance().isPrivateStorageProfileFromInvite()) {
                binding.newInviteButton.setVisibility(View.VISIBLE);
                binding.newInviteButton.setOnClickListener(view -> {
                    accountSetupActivity.onFlowSelected(Flow.INVITE_RESPOND);
                    accountSetupActivity.moveToPage(1);
                });
                binding.hostedAccountsButton.setVisibility(View.VISIBLE);
                binding.hostedAccountsButton.setOnClickListener(view -> {
                    accountSetupActivity.onFlowSelected(Flow.HOSTED_ACCOUNTS);
                    accountSetupActivity.moveToPage(1);
                });
                Threads.submitBackgroundTask(() -> CwebService.getInstance().checkForNewHostedProfiles());
            } else {
                binding.newInviteButton.setVisibility(View.GONE);
                binding.hostedAccountsButton.setVisibility(View.GONE);
            }

            binding.backgroundRestrictedMessage.setVisibility(accountSetupActivity.isBackgroundRestricted() ? View.VISIBLE : View.GONE);

            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            return false;
        }
    }

    public static class InviteRespond1Fragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountInviteRespond1Binding binding;
        private StorageProfileRequest request;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity) requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_invite_respond1, container, false);

            binding.inviteRespondInput.addTextChangedListener(new TextWatcher() {
                @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void afterTextChanged(Editable editable) {
                    StorageProfileRequest newRequest = CwebService.getInstance().parseInviteRequestText(editable.toString());
                    if (newRequest == null) {
                        binding.inviteRespondInput.setError("Invalid request");
                        binding.inviteRespondInput.requestFocus();
                        binding.inviteRespondFromNickname.setText("");
                        request = null;
                        return;
                    }
                    if (newRequest.equals(request)) {
                        return;
                    }
                    binding.inviteRespondInput.setError(null);
                    request = newRequest;
                    binding.inviteRespondFromNickname.setText(newRequest.getFromNickname());
                }
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            if (request == null) {
                binding.inviteRespondInput.setError("Invalid invite");
                binding.inviteRespondInput.requestFocus();
                return false;
            }
            accountSetupActivity.inviteRequest = request;
            accountSetupActivity.inviteResponseSubdir = StringUtils.trim(binding.inviteRespondSubdir.getText().toString());
            return true;
        }
    }

    public static class InviteRespond2Fragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebAccountInviteRespond2Binding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity) requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_account_invite_respond2, container, false);

            binding.inviteRespondResponseText.setOnFocusChangeListener((view, focus) -> {
                if (!focus) {
                    return;
                }
                CharSequence text = binding.inviteRespondResponseText.getText();
                if (text.length() == 0) {
                    return;
                }
                ClipboardManager clipboard = (ClipboardManager) accountSetupActivity.getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("StoneAge invite", text);
                clipboard.setPrimaryClip(clip);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                    Toast.makeText(accountSetupActivity, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
                }
            });
            binding.inviteRespondShare.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "StoneAge invite");
                intent.putExtra(Intent.EXTRA_TEXT, binding.inviteRespondResponseText.getText().toString());
                this.startActivity(Intent.createChooser(intent, "Send invite to..."));
            });

            return binding.getRoot();
        }

        @Override
        void onSelected() {
            StorageProfileRequest request = accountSetupActivity.inviteRequest;
            if (request != null) {
                String response = CwebService.getInstance().generateInviteText(request, accountSetupActivity.inviteResponseSubdir);
                binding.inviteRespondResponseText.setText(response);
            } else {
                binding.inviteRespondResponseText.setText("");
            }
        }

        @Override
        boolean onNext() {
            return true;
        }

        @Override
        int overrideNextButtonName() {
            return R.string.done;
        }
    }

    public static class HostedAccountsFragment extends AccountSetupFragment {
        private CwebAccountSetupActivity accountSetupActivity;
        private FragmentCwebHostedAccountsBinding binding;

        private View createRow(String nickname, String createdAt, String lastSeenAt, String idStr, boolean isHeading) {
            View row = getLayoutInflater().inflate(R.layout.cweb_hosted_account_row, null, false);
            TextView nicknameView = row.findViewById (R.id.nickname);
            nicknameView.setText(nickname);
            TextView createdAtView = row.findViewById (R.id.created_at);
            createdAtView.setText(createdAt);
            TextView lastSeenAtView = row.findViewById (R.id.last_seen_at);
            lastSeenAtView.setText(lastSeenAt);
            TextView idView = row.findViewById (R.id.cweb_id);
            idView.setText(idStr);
            if (isHeading) {
                nicknameView.setTextAppearance(accountSetupActivity, R.style.TextAppearance_Conversations_Caption_Highlight);
                createdAtView.setTextAppearance(accountSetupActivity, R.style.TextAppearance_Conversations_Caption_Highlight);
                lastSeenAtView.setTextAppearance(accountSetupActivity, R.style.TextAppearance_Conversations_Caption_Highlight);
                idView.setTextAppearance(accountSetupActivity, R.style.TextAppearance_Conversations_Caption_Highlight);
            }
            return row;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            accountSetupActivity = ((CwebAccountSetupActivity) requireActivity());
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cweb_hosted_accounts, container, false);
            View headingRow = createRow("Nickname", "Created", "Last Seen", "ID", true);
            binding.accountTable.addView(headingRow);

            List<CwebService.HostedProfileProperties> hostedProfiles = CwebService.getInstance().getHostedProfiles();
            for (CwebService.HostedProfileProperties hostedProfile : hostedProfiles) {
                View row = createRow(hostedProfile.nickname, hostedProfile.createdAt, hostedProfile.lastSeenAt, hostedProfile.idStr, false);
                binding.accountTable.addView(row);
            }

            return binding.getRoot();
        }

        @Override
        void onSelected() {}

        @Override
        boolean onNext() {
            return true;
        }

        @Override
        int overrideNextButtonName() {
            return R.string.done;
        }
    }
}
