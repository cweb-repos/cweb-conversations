package eu.siacs.conversations.cweb;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.StrictMode;
import android.util.Pair;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.cweb.AccountUtils;
import org.cweb.ApplicationDataStore;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerBase;
import org.cweb.identity.IdentityService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.admin.LocalHostedProfile;
import org.cweb.schemas.admin.StorageProfileRequest;
import org.cweb.schemas.admin.StorageProfileResponse;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.wire.CryptoEnvelope;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.local.SecretStorageService;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.PropertyUtils;
import org.cweb.utils.ThriftTextUtils;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.simple.SimpleLogger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import eu.siacs.conversations.BuildConfig;
import eu.siacs.conversations.cweb.schemas.LocalState;
import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.xmpp.Jid;

public class CwebService {
    private static final Logger log;
    private static final String LOCAL_STATE_NAME = "localState";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    private static volatile CwebService instance = null;

    private String localRootPath;
    private InstanceContainerBase instanceContainerBase;
    private InstanceContainer instanceContainer;
    private ApplicationDataStore<LocalState> localStateApplicationDataStore;
    private PrivateStorageProfile embeddedPrivateStorageProfile;
    private CwebMessages cwebMessages;
    private XmppAdapter xmppAdapter;

    private XmppConnectionService xmppConnectionService;

    static {
        System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, isDebugBuild() ? "TRACE" : "WARN");
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "true");
        System.setProperty(SimpleLogger.SHOW_THREAD_NAME_KEY, "true");
        System.setProperty(SimpleLogger.DATE_TIME_FORMAT_KEY, "yyyy-MM-dd'T'HH:mm:ss.SSS");
        log = LoggerFactory.getLogger(CwebService.class);

        if (isDebugBuild()) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }
    }

    public static CwebService getInstance() {
        CwebService localInstance = instance;
        if (localInstance == null) {
            synchronized (CwebService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new CwebService();
                }
            }
        }
        return localInstance;
    }

    public static CwebMessages getMessages() {
        return getInstance().cwebMessages;
    }

    public static XmppAdapter getAdapter() {
        return getInstance().xmppAdapter;
    }

    private CwebService() {
    }

    private static String getLocalRootPath(String appRootDir) {
        return appRootDir + "/cweb";
    }

    public synchronized void initLocalState(Context context, String appRootDir) {
        if (localStateApplicationDataStore != null) {
            return;
        }

        localRootPath = getLocalRootPath(appRootDir);
        localStateApplicationDataStore = AccountUtils.getApplicationDataStore(localRootPath, LocalState.class, LOCAL_STATE_NAME);
        embeddedPrivateStorageProfile = extractEmbeddedStorageProfile(context);
    }

    public synchronized void loadOrCreateLocalIdentity() {
        if (instanceContainerBase != null) {
            return;
        }

        LocalState localState = getLocalState();
        if (localState == null) {
            byte[] secretStorageKey = SecretStorageService.generateKey();
            instanceContainerBase = AccountUtils.createNewIdentity(localRootPath, secretStorageKey);
            localState = new LocalState();
            localState.setId(instanceContainerBase.getOwnId());
            localState.setSecretStorageKey(secretStorageKey);
            localStateApplicationDataStore.put(localState);
        } else {
            byte[] id = localState.getId();
            instanceContainerBase = new InstanceContainerBase(localRootPath, id, localState.getSecretStorageKey());
        }
    }

    public synchronized void initAccount(final Account account, final XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;

        LocalState localState = getLocalState();
        byte[] id = localState.getId();

        if (instanceContainer != null) {
            byte[] idFromAccount = JidUtils.getCwebId(account.getJid());
            if (!Arrays.equals(idFromAccount, id)) {
                log.error("re-initializing with different account " + Utils.getDebugStringFromId(idFromAccount) + " != " + Utils.getDebugStringFromId(id));
                Preconditions.checkArgument(false);
            }

            cwebMessages.setXmppConnectionService(xmppConnectionService);
            xmppAdapter.setXmppConnectionService(xmppConnectionService);
            return;
        }

        // After backup restore the account is disabled
        if (!account.isEnabled()) {
            account.setOption(Account.OPTION_DISABLED, false);
        }

        instanceContainerBase = instanceContainer = new InstanceContainer(localRootPath, id, localState.getSecretStorageKey());

        Property nicknameProperty = createNicknameProperty(account.getDisplayName());
        instanceContainer.getIdentityService().updateProperties(Lists.newArrayList(nicknameProperty));
        instanceContainer.getIdentityProfilePostService().updateProperties(Lists.newArrayList(
                nicknameProperty,
                new Property("MANUFACTURER", PropertyValue.str(Build.MANUFACTURER)),
                new Property("BRAND", PropertyValue.str(Build.BRAND)),
                new Property("MODEL", PropertyValue.str(Build.MODEL))));

        this.cwebMessages = new CwebMessages(instanceContainer, xmppConnectionService);
        this.xmppAdapter = new XmppAdapter(this, cwebMessages, instanceContainer, xmppConnectionService);
    }

    static String getNicknameProperty(List<Property> properties) {
        return PropertyUtils.getStringProperty(properties, PropertyUtils.PROPERTY_KEY_NICK_NAME);
    }

    static Property createNicknameProperty(String nickName) {
        return new Property(PropertyUtils.PROPERTY_KEY_NICK_NAME, PropertyValue.str(nickName));
    }

    static String getSubjectProperty(List<Property> properties) {
        return PropertyUtils.getStringProperty(properties, Constants.SHARE_SESSION_PROPERTY_KEY_CONFERENCE_SUBJECT);
    }

    static Property createSubjectProperty(String subject) {
        return new Property(Constants.SHARE_SESSION_PROPERTY_KEY_CONFERENCE_SUBJECT, PropertyValue.str(subject));
    }

    static FileReference getProfilePictureProperty(List<Property> properties) {
        return PropertyUtils.getFileReferenceProperty(properties, PropertyUtils.PROPERTY_KEY_PROFILE_PICTURE);
    }

    static Property createProfilePictureProperty(FileReference fileReference) {
        TypedPayload payload = TypedPayloadUtils.wrap(fileReference, Constants.SERVICE_NAME, null, null);
        return new Property(PropertyUtils.PROPERTY_KEY_PROFILE_PICTURE, PropertyValue.typedPayload(payload));
    }

    public Logger getLog() {
        return log;
    }

    LocalState getLocalState() {
        return localStateApplicationDataStore.get();
    }

    synchronized void updateLocalState(LocalStateUpdater updater) {
        LocalState localState = localStateApplicationDataStore.get();
        boolean updated = updater.update(localState);
        if (updated) {
            localStateApplicationDataStore.put(localState);
        }
    }

    public synchronized void updateNickname(String nickname) {
        Property nicknameProperty = createNicknameProperty(nickname);
        instanceContainer.getIdentityService().updateProperties(Lists.newArrayList(nicknameProperty));
        instanceContainer.getIdentityProfilePostService().updateProperties(Lists.newArrayList(nicknameProperty));
    }

    public String getPublicStorageProfileStr() {
        return StorageProfileUtils.toHumanReadableString(instanceContainer.getPublicStorageProfile());
    }

    public synchronized Pair<Account, String> createCwebAccount(XmppConnectionService xmppConnectionService, String nickname, PrivateStorageProfile privateStorageProfile, boolean fromEmbedded, StorageProfileResponse invite) {
        byte[] id = getLocalState().getId();
        boolean success = Threads.runAsNewThreadBlocking(() -> {
            InstanceContainer instanceContainer = AccountUtils.attachPrivateStorageProfile(instanceContainerBase, privateStorageProfile);
            return instanceContainer != null;
        });
        if (!success) {
            return Pair.create(null, "Failed to create account, check your network and storage profile");
        }

        onPrivateStorageProfileUpdated(fromEmbedded, invite);

        Jid jid = JidUtils.constructContactJid(id, false);
        Account account = new Account(jid, "");
        account.setDisplayName(nickname);
        account.setOption(Account.OPTION_LOGGED_IN_SUCCESSFULLY, true);
        xmppConnectionService.createAccount(account);
        if (!xmppConnectionService.updateAccount(account)) {
            return Pair.create(account, "Failed to update account");
        }

        initAccount(account, xmppConnectionService);
        getAdapter().addAdminAsContact();
        CwebService.getMessages().initCommScheduler();

        return Pair.create(account, null);
    }

    public synchronized org.apache.commons.lang3.tuple.Pair<Boolean, String> updatePrivateStorageProfile(PrivateStorageProfile privateStorageProfile, StorageProfileResponse invite) {
        if (privateStorageProfile == null) {
            return org.apache.commons.lang3.tuple.Pair.of(false, "Invalid storage profile");
        }

        org.apache.commons.lang3.tuple.Pair<Boolean, String> result = Threads.runAsNewThreadBlocking(() ->
                instanceContainer.updatePrivateStorageProfile(privateStorageProfile));
        if (result.getLeft()) {
            onPrivateStorageProfileUpdated(false, invite);
        }

        return result;
    }

    private void onPrivateStorageProfileUpdated(boolean fromEmbedded, StorageProfileResponse invite) {
        updateLocalState(state -> {
            state.setPrivateStorageProfileFromEmbedded(fromEmbedded);
            if (invite != null) {
                state.setStorageProfileInviteAdminIdentityReference(ThriftUtils.serialize(invite.getAdmin()));
            } else {
                state.unsetStorageProfileInviteAdminIdentityReference();
            }
            return true;
        });
    }

    public static PrivateStorageProfile extractAccountStr(String cwebAccount) {
        String privateStorageProfileStr;
        int accountPrefixIdx = cwebAccount.indexOf(Constants.ACCOUNT_PREFIX);
        if (accountPrefixIdx == -1) {
            privateStorageProfileStr = cwebAccount;
        } else {
            privateStorageProfileStr = cwebAccount.substring(accountPrefixIdx + Constants.ACCOUNT_PREFIX.length());
        }
        return parsePrivateStorageProfile(privateStorageProfileStr);
    }

    private static PrivateStorageProfile parsePrivateStorageProfile(String profileStr) {
        PrivateStorageProfile profile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(profileStr);
        if (profile != null) {
            return profile;
        }
        return ThriftTextUtils.fromUrlSafeString(profileStr, PrivateStorageProfile.class);
    }

    public void onPhoneStateChange() {
        if (cwebMessages == null || xmppConnectionService == null || !xmppConnectionService.hasInternetConnection()) {
            return;
        }
        cwebMessages.onPhoneStateChange();
    }

    private static boolean isDebugBuild() {
        return "debug".equals(BuildConfig.BUILD_TYPE);
    }

    static String idToString(byte[] id) {
        Preconditions.checkArgument(IdentityService.isValidId(id));
        return Utils.toBase32String(id);
    }

    static String sharedSessionIdToString(byte[] id) {
        return Utils.toBase32String(id);
    }

    public static byte[] idFromString(String idStr) {
        byte[] id = Utils.fromBase32String(idStr);
        if (!IdentityService.isValidId(id)) {
            return null;
        }
        return id;
    }

    public void deleteInboundCache() {
        if (instanceContainerBase != null) {
            instanceContainerBase.deleteInboundStorageCache();
        }
    }

    private PrivateStorageProfile extractEmbeddedStorageProfile(Context context) {
        Resources res = context.getResources();
        int id = res.getIdentifier("private_storage_profile_base", "string", context.getPackageName());
        if (id == 0) {
            return null;
        }
        String storageProfileStr = res.getString(id);
        return parsePrivateStorageProfile(storageProfileStr);
    }

    public PrivateStorageProfile getEmbeddedStorageProfile() {
        if (embeddedPrivateStorageProfile == null) {
            return null;
        }
        PrivateStorageProfile currentStorageProfile = instanceContainer != null ? instanceContainer.getPrivateStorageProfile() : null;
        boolean privateStorageProfileFromEmbedded = getLocalState().isPrivateStorageProfileFromEmbedded();
        boolean isUsingEmbeddedStorageProfile = currentStorageProfile == null || privateStorageProfileFromEmbedded;
        return isUsingEmbeddedStorageProfile ? embeddedPrivateStorageProfile : null;
    }

    public boolean canEditPrivateStorageProfile() {
        return getEmbeddedStorageProfile() == null;
    }

    public boolean canSharePrivateStorageProfile() {
        return instanceContainer != null && instanceContainer.getPrivateStorageProfile() != null && canEditPrivateStorageProfile() && !isPrivateStorageProfileFromInvite();
    }

    public String getInviteRequestText(String nickname) {
        CryptoEnvelope request = instanceContainerBase.getRemoteAdminClientService().generateStorageProfileRequest(nickname);
        return ThriftTextUtils.formatShareableString(ThriftTextUtils.toUrlSafeString(request), "StoneAgeInviteRequest", 30);
    }

    public StorageProfileRequest parseInviteRequestText(String inviteRequest) {
        String requestStr = ThriftTextUtils.parseShareableString(inviteRequest);
        if (requestStr == null) {
            return null;
        }
        byte[] requestBin = ThriftTextUtils.fromUrlSafeString(requestStr);
        if (requestBin == null) {
            return null;
        }
        return instanceContainer.getRemoteAdminHostService().decodeStorageProfileRequest(requestBin);
    }

    public String generateInviteText(StorageProfileRequest request, String suffix) {
        final PrivateStorageProfile basePrivateStorageProfile = instanceContainer.getPrivateStorageProfile();
        PrivateStorageProfile invitePrivateStorageProfile = !StringUtils.isBlank(suffix) ? StorageProfileUtils.appendToPaths(basePrivateStorageProfile, suffix) : basePrivateStorageProfile;
        CryptoEnvelope response = instanceContainer.getRemoteAdminHostService().generateStorageProfileResponse(request, invitePrivateStorageProfile, instanceContainer.getIdentityService().getOwnIdentityReference());
        return ThriftTextUtils.formatShareableString(ThriftTextUtils.toUrlSafeString(response), "StoneAgeInvite", 30);
    }

    public StorageProfileResponse parseInviteResponseText(String inviteResponse) {
        String responseStr = ThriftTextUtils.parseShareableString(inviteResponse);
        if (responseStr == null) {
            return null;
        }
        byte[] responseBin = ThriftTextUtils.fromUrlSafeString(responseStr);
        if (responseBin == null) {
            return null;
        }
        return Threads.runAsNewThreadBlocking(() -> {
            return instanceContainerBase.getRemoteAdminClientService().decodeStorageProfileResponse(responseBin, instanceContainerBase.getRemoteIdentityFetcher());
        });
    }

    public boolean isPrivateStorageProfileFromInvite() {
        return getLocalState().isSetStorageProfileInviteAdminIdentityReference();
    }

    public String getNickname(IdentityReference identityReference) {
        LocalIdentityDescriptorState identityDescriptorState = instanceContainerBase.getRemoteIdentityFetcher().fetch(identityReference);
        if (identityDescriptorState == null) {
            return null;
        }
        List<Property> properties = identityDescriptorState.getDescriptor().getOwnProperties();
        return getNicknameProperty(properties);
    }

    public void addInvitedAsContact(IdentityReference identityRef) {
        xmppAdapter.addContact(identityRef);
    }

    public void checkForNewHostedProfiles() {
        instanceContainer.getCommScheduler().checkForNewHostedProfiles(true);
    }

    public static class HostedProfileProperties {
        public String nickname;
        public String createdAt;
        public String lastSeenAt;
        public String idStr;
    }

    public List<HostedProfileProperties> getHostedProfiles() {
        List<LocalHostedProfile> profiles = instanceContainer.getRemoteAdminHostService().getLocalHostedProfiles();
        List<HostedProfileProperties> result = new ArrayList<>();

        for (LocalHostedProfile profile : profiles) {
            HostedProfileProperties item = new HostedProfileProperties();
            byte[] id = profile.getRequest().getId();
            item.idStr = idToString(id).substring(0, 8) + "...";
            item.nickname = profile.getRequest().getFromNickname();
            item.createdAt = "N/A";
            item.lastSeenAt = "N/A";
            if (profile.isSetFirstSeenAt()) {
                item.createdAt = DATE_FORMAT.format(new Date(profile.getFirstSeenAt()));
                LocalIdentityDescriptorState identityDescriptor = instanceContainer.getRemoteIdentityService().get(id);
                if (identityDescriptor != null) {
                    item.nickname = StringUtils.firstNonBlank(getNicknameProperty(identityDescriptor.getDescriptor().getOwnProperties()), item.nickname);
                }
                item.lastSeenAt = DATE_FORMAT.format(new Date(identityDescriptor.getSignedAt()));
            }

            result.add(item);
        }

        return result;
    }

    public byte[] createBackup(String password) {
        byte[] key = Arrays.copyOf(password.getBytes(StandardCharsets.UTF_8), 32);
        ByteArrayOutputStream backupOut = new ByteArrayOutputStream();
        AccountUtils.backup(instanceContainer, backupOut, key, ThriftUtils.serialize(getLocalState()));
        return backupOut.toByteArray();
    }

    public boolean restoreBackup(byte[] cwebBackup, String password) {
        byte[] key = Arrays.copyOf(password.getBytes(StandardCharsets.UTF_8), 32);
        ByteArrayInputStream backupIn = new ByteArrayInputStream(cwebBackup);
        org.apache.commons.lang3.tuple.Pair<byte[], byte[]> restoreResult = AccountUtils.restore(localRootPath, backupIn, key);

        if (restoreResult == null) {
            return false;
        }

        if (restoreResult.getRight() != null) {
            localStateApplicationDataStore.put(ThriftUtils.deserialize(restoreResult.getRight(), LocalState.class));
        }
        return true;
    }

    public String generateBackupPassword() {
        return RandomStringUtils.randomAlphanumeric(8);
    }
}
