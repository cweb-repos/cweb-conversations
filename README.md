<h1 align="center">StoneAge Messenger</h1>

A simple P2P messenger app, with no central backend - requires user-provided S3 bucket to function.

All data is stored on your device and in your S3 bucket. Messages are end-to-end encrypted and exchanged via a P2P protocol over S3.

No phone number is required.

Supports text, images, audio, video. No voice or video calls.

For more details see https://cweb.gitlab.io/StoneAge.html

Forked from [Conversations](https://github.com/siacs/Conversations)
with communication protocol switched from XMPP to [Cweb](https://cweb.gitlab.io).
