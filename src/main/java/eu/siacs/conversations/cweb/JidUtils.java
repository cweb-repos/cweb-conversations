package eu.siacs.conversations.cweb;

import android.util.Log;

import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.utils.Utils;

import java.nio.ByteBuffer;

import eu.siacs.conversations.Config;
import eu.siacs.conversations.xmpp.Jid;

public class JidUtils {
    private static final String DOMAIN_SHARED_SESSION = "cwebsharedsession";
    private static final String DOMAIN_CONTACT = "cweb";

    public static Jid constructContactJid(byte[] id, boolean ignoreExceptions) {
        return constructContactJid(id, null, ignoreExceptions);
    }

    public static Jid constructContactJid(byte[] id, String resource, boolean ignoreExceptions) {
        try {
            String idStr = CwebService.idToString(id);
            return Jid.of(idStr, DOMAIN_CONTACT, resource);
        } catch (Exception e) {
            if (ignoreExceptions) {
                CwebService.getInstance().getLog().error("Failed to construct jid", e);
                return null;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static Jid constructConferenceJid(byte[] sessionId, boolean ignoreExceptions) {
        try {
            String sessionIdStr = Utils.toBase32String(sessionId);
            return Jid.of(sessionIdStr, DOMAIN_SHARED_SESSION, null);
        } catch (Exception e) {
            if (ignoreExceptions) {
                CwebService.getInstance().getLog().error("Failed to construct jid", e);
                return null;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static Jid constructJid(SessionId sessionId, boolean ignoreExceptions) {
        if (sessionId.getType() == SessionType.COMM_SESSION) {
            return constructContactJid(sessionId.getId(), ignoreExceptions);
        } else if (sessionId.getType() == SessionType.SHARED_SESSION) {
            return constructConferenceJid(sessionId.getId(), ignoreExceptions);
        }
        return null;
    }

    private static boolean isCwebDomain(Jid jid) {
        return DOMAIN_CONTACT.equals(jid.getDomain().toString());
    }

    private static boolean isCwebSharedSessionDomain(Jid jid) {
        return DOMAIN_SHARED_SESSION.equals(jid.getDomain().toString());
    }

    public static boolean isCwebContact(Jid jid) {
        return getCwebId(jid) != null;
    }

    public static boolean isCwebSharedSession(Jid jid) {
        return isCwebSharedSessionDomain(jid) && getCwebSharedSessionId(jid) != null;
    }

    public static byte[] getCwebId(Jid jid) {
        if (!isCwebDomain(jid)) {
            return null;
        }
        byte[] cwebId = CwebService.idFromString(jid.getLocal());
        if (cwebId == null) {
            Log.i(Config.LOGTAG, "Error extracting cweb id from " + jid.toString());
        }
        return cwebId;
    }

    public static byte[] getCwebSharedSessionId(Jid jid) {
        if (!isCwebSharedSessionDomain(jid)) {
            return null;
        }
        byte[] sessionId = Utils.fromBase32String(jid.getLocal());
        if (sessionId == null) {
            Log.i(Config.LOGTAG, "Error extracting cweb shared session id from " + jid.toString());
        }
        return sessionId;
    }

    public static SessionId getCwebSessionId(Jid jid) {
        if (isCwebDomain(jid)) {
            byte[] cwebId = CwebService.idFromString(jid.getLocal());
            if (cwebId == null) {
                Log.i(Config.LOGTAG, "Error extracting cweb id from " + jid.toString());
                return null;
            }
            return new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(cwebId));
        } else if (isCwebSharedSessionDomain(jid)) {
            byte[] sessionId = Utils.fromBase32String(jid.getLocal());
            if (sessionId == null) {
                Log.i(Config.LOGTAG, "Error extracting cweb shared session id from " + jid.toString());
                return null;
            }
            return new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId));
        } else {
            Log.i(Config.LOGTAG, "Error extracting cweb session id from " + jid.toString());
            return null;
        }
    }

    public static Jid stringToJidSafe(String jidStr) {
        if (jidStr == null) {
            return null;
        }
        try {
            return Jid.of(jidStr);
        } catch (IllegalArgumentException ignored) {
        }
        return null;
    }
}
