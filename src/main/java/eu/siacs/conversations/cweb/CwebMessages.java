package eu.siacs.conversations.cweb;

import android.os.SystemClock;
import android.util.Pair;

import org.apache.commons.lang3.tuple.Triple;
import org.cweb.InstanceContainer;
import org.cweb.communication.CommSessionMessageCallback;
import org.cweb.communication.CommSessionService;
import org.cweb.communication.NativeSchedulingProvider;
import org.cweb.communication.SharedSessionCallback;
import org.cweb.communication.SharedSessionService;
import org.cweb.files.FileDownloadService;
import org.cweb.files.FileUploadService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.schemas.wire.PayloadType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.schemas.wire.TypedPayloadMetadata;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledFuture;

import eu.siacs.conversations.entities.Conversation;
import eu.siacs.conversations.entities.Message;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.xmpp.Jid;
import eu.siacs.conversations.xmpp.XmppConnection;

public class CwebMessages {
    private static final long MIN_CHECK_INTERVAL = 1000L * 5;
    private static final String CUSTOM_PAYLOAD_TYPE_MESSAGE = "Message";

    private final Logger log;
    private final InstanceContainer instanceContainer;
    private XmppConnectionService xmppConnectionService;
    private final Set<SessionId> sessions = new LinkedHashSet<>();
    private final LinkedBlockingDeque<MessageUpdate> messageUpdates = new LinkedBlockingDeque<>();

    private static long maxCheckInterval = 1000L * 60 * 15;

    private static class MessageUpdate {
        final UpdateType type;
        final SessionId sessionId;
        final long createdAt;
        final TypedPayload payload;

        enum UpdateType {
            RECEIVED,
            ACK
        }

        private MessageUpdate(UpdateType type, CommSessionService.ReceivedMessage direct) {
            this.type = type;
            this.sessionId = new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(direct.fromId));
            this.createdAt = direct.createdAt;
            this.payload = direct.payload;
        }

        private MessageUpdate(UpdateType type, byte[] sessionId, SharedSessionService.ReceivedMessage shared) {
            this.type = type;
            this.sessionId = new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId));
            this.createdAt = shared.createdAt;
            this.payload = shared.payload;
        }
    }

    CwebMessages(InstanceContainer instanceContainer, final XmppConnectionService xmppConnectionService) {
        this.log = LoggerFactory.getLogger(CwebMessages.class);
        this.instanceContainer = instanceContainer;
        this.xmppConnectionService = xmppConnectionService;

        Thread messageQueueHandler = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        MessageUpdate message = messageUpdates.take();
                        log.trace("Received message " + message.type.name() + " from " + message.sessionId.getType().name() + ":" + org.cweb.utils.Utils.getDebugStringFromId(message.sessionId.getId()));
                        processMessage(message);
                    } catch (InterruptedException e) {
                    }
                }
            }
        };
        messageQueueHandler.setDaemon(true);
        messageQueueHandler.start();

        instanceContainer.getCommSessionService().addCommSessionEstablishmentCallback(withId -> CwebService.getAdapter().addContact(withId));

        instanceContainer.getCommScheduler().setCommSessionMessageCallback(commSessionMessageCallback);

        instanceContainer.getCommScheduler().setSharedSessionCallback(sharedSessionCallback);

        instanceContainer.getIdentityProfileReadService().addUpdateCallback((fromId, identityProfile) -> CwebService.getAdapter().updateContactFromProfileOrIdentity(fromId));

        Threads.submitBackgroundTask(this::garbageCollectOldUploads);
    }

    public void setXmppConnectionService(XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;
    }

    public void initCommScheduler() {
        instanceContainer.getCommScheduler().init(schedulingProvider);
    }

    private void garbageCollectOldUploads() {
        long now = System.currentTimeMillis();
        List<byte[]> uploadedFiles = CwebMessages.this.instanceContainer.getFileUploadService().list();
        for (byte[] fileId : uploadedFiles) {
            FileMetadata metadata = CwebMessages.this.instanceContainer.getFileUploadService().getMetadata(fileId);
            String fileType = PropertyUtils.getStringProperty(metadata.properties, Constants.FILE_PROPERTY_TYPE_KEY);
            boolean shouldDeleteByType = !Constants.FILE_PROPERTY_TYPE_AVATAR.equals(fileType);
            boolean shouldDeleteByAge = now - metadata.getCreatedAt() > Constants.UPLOADED_FILES_TTL;
            if (shouldDeleteByType && shouldDeleteByAge) {
                log.trace("Deleting old upload " + NameConversionUtils.toString(fileId) + ": " + metadata);
                CwebMessages.this.instanceContainer.getFileUploadService().delete(fileId, false);
            }
        }
    }

    private final CommSessionMessageCallback commSessionMessageCallback = new CommSessionMessageCallback() {
        private static final long FETCH_PROFILE_INTERVAL = 1000L * 60 * 60 * 2;
        private final Map<ByteBuffer, Long> lastTimeFetchedProfile = new ConcurrentHashMap<>();

        void requestProfileFetch(byte[] fromId) {
            instanceContainer.getIdentityProfileReadService().requestProfileFetch(fromId);
        }

        @Override
        public void onMessagesReceived(byte[] fromId) {
            // Consume the acks, even though the messenger doesn't use them and relies
            // on explicit XMPP messages for delivery notifications
            List<CommSessionService.ReceivedMessage> messagesAcked = instanceContainer.getCommSessionService().fetchAckedMessages(fromId, true);
            /*for (CommSessionService.ReceivedMessage message : messagesAcked) {
                messageUpdates.add(new MessageUpdate(MessageUpdate.UpdateType.ACK, message));
            }*/

            List<CommSessionService.ReceivedMessage> messagesReceived = instanceContainer.getCommSessionService().fetchNewMessages(fromId, true);
            if (messagesReceived == null) {
                return;
            }

            long now = SystemClock.elapsedRealtime();
            ByteBuffer fromIdWrapped = ByteBuffer.wrap(fromId);
            Long lastUpdated = lastTimeFetchedProfile.get(fromIdWrapped);
            if (lastUpdated == null || now - lastUpdated > FETCH_PROFILE_INTERVAL) {
                requestProfileFetch(fromId);
                lastTimeFetchedProfile.put(fromIdWrapped, now);
            }

            List<MessageUpdate> messageUpdatesTmp = new ArrayList<>();
            for (CommSessionService.ReceivedMessage message : messagesReceived) {
                messageUpdatesTmp.add(new MessageUpdate(MessageUpdate.UpdateType.RECEIVED, message));
            }
            messageUpdates.addAll(messageUpdatesTmp);
        }
    };

    private SharedSessionCallback sharedSessionCallback = new SharedSessionCallback() {
        @Override
        public void onMessagesReceived(byte[] sessionId) {
            List<SharedSessionService.ReceivedMessage> messages = instanceContainer.getSharedSessionService().fetchNewMessages(sessionId, true);
            if (messages != null) {
                for (SharedSessionService.ReceivedMessage message : messages) {
                    messageUpdates.add(new MessageUpdate(MessageUpdate.UpdateType.RECEIVED, sessionId, message));
                }
            }
        }

        @Override
        public void onMessagesAcked(byte[] sessionId) {
            List<SharedSessionService.ReceivedMessage> messages = instanceContainer.getSharedSessionService().fetchAckedMessages(sessionId, true);
            if (messages != null) {
                for (SharedSessionService.ReceivedMessage message : messages) {
                    messageUpdates.add(new MessageUpdate(MessageUpdate.UpdateType.ACK, sessionId, message));
                }
            }
        }

        @Override
        public void onDescriptorUpdated(byte[] sessionId) {
            SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
            boolean isUnsubscribed = metadata.isUnsubscribed;
            Jid jid = JidUtils.constructConferenceJid(sessionId, false);
            Conversation conversation = xmppConnectionService.find(CwebService.getAdapter().getAccount(), jid);
            if (conversation != null) {
                CwebService.getAdapter().updateConferenceFromDescriptor(sessionId, metadata, conversation);
            } else if (!isUnsubscribed) {
                conversation = xmppConnectionService.findOrCreateConversation(CwebService.getAdapter().getAccount(), jid, true, false);
                CwebService.getAdapter().addConference(conversation);
            }
            CwebService.getAdapter().updateSelfRoleInConference(sessionId);
            xmppConnectionService.updateConversationUi();

            if (!isUnsubscribed) {
                requestPeriodicSync(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(metadata.sessionId)));
            } else {
                cancelPeriodicSync(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(metadata.sessionId)));
            }
        }
    };

    private NativeSchedulingProvider.Callbacks schedulingProviderCallbacks;

    private NativeSchedulingProvider schedulingProvider = new NativeSchedulingProvider() {
        private ScheduledFuture<?> wakeupTask;

        @Override
        public long getTime() {
            return SystemClock.elapsedRealtime();
        }

        @Override
        public long getNoConnectionRetryInterval() {
            return maxCheckInterval;
        }

        @Override
        public boolean hasInternetConnection() {
            return xmppConnectionService.hasInternetConnection();
        }

        @Override
        public synchronized void scheduleWakeup(long time) {
            cancelWakeup();
            long now = getTime();
            long delay = Math.max(0, time - now);
            this.wakeupTask = org.cweb.utils.Threads.submitTask(new Runnable() {
                @Override
                public void run() {
                    wakeupTask = null;
                    wakeMessageReadScheduler();
                }
            }, delay);

            // Adding 1s to reduce jitter
            xmppConnectionService.scheduleNextIdlePingFor(time + 1000);
        }

        @Override
        public synchronized void cancelWakeup() {
            xmppConnectionService.cancelNextIdlePing();

            if (wakeupTask != null && !wakeupTask.isDone() && !wakeupTask.isCancelled()) {
                wakeupTask.cancel(false);
            }
        }

        @Override
        public void setCallbacks(Callbacks callbacks) {
            schedulingProviderCallbacks = callbacks;
        }
    };

    public static long getMaxCheckInterval() {
        return maxCheckInterval;
    }

    public static void setMaxCheckInterval(long value) {
        if (value == 0) {
            value = Long.MAX_VALUE;
        }
        maxCheckInterval = value;

        CwebMessages instance = CwebService.getMessages();
        if (instance != null) {
            instance.refreshSyncIntervals();
        }
    }

    public void refreshSyncIntervals() {
        for (SessionId sessionId : sessions) {
            requestPeriodicSync(sessionId);
        }
    }

    public void reportUserInteraction(Conversation conversation) {
        SessionId sessionId = JidUtils.getCwebSessionId(conversation.getJid());
        if (sessionId == null) {
            return;
        }
        instanceContainer.getCommScheduler().reportInteraction(sessionId);
    }

    public void wakeMessageReadScheduler() {
        schedulingProviderCallbacks.wakeUp();
    }

    synchronized void setSessions(List<SessionId> sessionIds) {
        this.sessions.clear();
        this.sessions.addAll(sessionIds);
        List<byte[]> directContacts = new ArrayList<>();
        for (SessionId sessionId : sessionIds) {
            if (sessionId.getType() == SessionType.COMM_SESSION) {
                directContacts.add(sessionId.getId());
            }
            requestPeriodicSync(sessionId);
        }
        Threads.submitBackgroundTask(() -> instanceContainer.getIdentityProfilePostService().setSubscribers(directContacts));
    }

    synchronized void addSession(SessionId sessionId) {
        sessions.add(sessionId);
        requestPeriodicSync(sessionId);
        if (sessionId.getType() == SessionType.COMM_SESSION) {
            Threads.submitBackgroundTask(() -> instanceContainer.getIdentityProfilePostService().addSubscriber(sessionId.getId()));
        }
    }

    synchronized void removeSession(SessionId sessionId) {
        sessions.remove(sessionId);
    }

    private void requestPeriodicSync(SessionId sessionId) {
        instanceContainer.getCommScheduler().requestPeriodicSync(sessionId, MIN_CHECK_INTERVAL, maxCheckInterval);
    }

    private void cancelPeriodicSync(SessionId sessionId) {
        instanceContainer.getCommScheduler().cancelPeriodicSync(sessionId);
    }

    public Pair<byte[], FileMetadata> startFileDownload(byte[] id, String fileUrl, String path) {
        if (!fileUrl.startsWith(Constants.URL_PREFIX_ATTACHMENT_FILE)) {
            return null;
        }
        byte[] fileId = org.cweb.utils.Utils.fromBase32String(fileUrl.substring(Constants.URL_PREFIX_ATTACHMENT_FILE.length(), Constants.URL_PREFIX_ATTACHMENT_FILE.length() + 56));

        org.apache.commons.lang3.tuple.Pair<FileMetadata, FileDownloadService.DownloadError> result = instanceContainer.getFileDownloadService().startDownload(id, fileId, path);
        if (result.getRight() != null) {
            return null;
        }

        return Pair.create(fileId, result.getLeft());
    }

    public Pair<byte[], String> startFileUpload(Message message, final String fullPath, String filename) {
        SessionId toId = JidUtils.getCwebSessionId(message.getConversation().getContact().getJid());
        List<Property> properties = Collections.singletonList(new Property(Constants.FILE_PROPERTY_TYPE_KEY, PropertyValue.str(Constants.FILE_PROPERTY_TYPE_ATTACHMENT)));
        Triple<LocalUploadedFileInfo, FileMetadata, FileUploadService.UploadError> result = instanceContainer.getFileUploadService().upload(fullPath, filename, properties, Constants.FILE_PART_SIZE_BYTES, false);
        if (result.getRight() != null) {
            log.warn("Failed to upload file attachment on message " + message.getUuid() + ", error=" + result.getRight());
            return null;
        }

        byte[] fileId = result.getLeft().getFileReference().getFileId();
        boolean sharedSuccessfully = instanceContainer.getFileUploadService().share(fileId, toId);
        if (!sharedSuccessfully) {
            log.warn("Failed to share attachment on message " + message.getUuid());
            instanceContainer.getFileUploadService().delete(fileId, false);
            return null;
        }

        String encodedFilename;
        try {
            encodedFilename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            log.warn("Failed to URL encode filename " + e.getMessage());
            return null;
        }
        return Pair.create(fileId, Constants.URL_PREFIX_ATTACHMENT_FILE + org.cweb.utils.Utils.toBase32String(fileId) + "." + encodedFilename);
    }

    public FileUploadService.UploadState getUploadState(byte[] fileId) {
        return instanceContainer.getFileUploadService().getUploadState(fileId);
    }

    public FileDownloadService.DownloadState getDownloadState(byte[] fromId, byte[] fileId) {
        return instanceContainer.getFileDownloadService().getDownloadState(fromId, fileId);
    }

    synchronized boolean sendMessageInternal(byte[] data, SessionId toSessionId, byte[] refId) {
        TypedPayload payload = TypedPayloadUtils.wrapCustom(data, Constants.SERVICE_NAME, CUSTOM_PAYLOAD_TYPE_MESSAGE, refId);
        byte[] toId = toSessionId.getId();
        if (toSessionId.getType() == SessionType.COMM_SESSION) {
            CommSessionService commSessionService = instanceContainer.getCommSessionService();
            if (!commSessionService.haveSessionWith(toId)) {
                commSessionService.establishSessionWith(toId);
            }
            return commSessionService.sendMessage(toId, payload);
        } else {
            return instanceContainer.getSharedSessionService().sendMessage(toId, payload);
        }
    }

    private void processMessage(MessageUpdate messageUpdate) {
        SessionId sessionId = messageUpdate.sessionId;
        TypedPayload typedPayload = messageUpdate.payload;
        TypedPayloadMetadata metadata = typedPayload.getMetadata();
        if (messageUpdate.type == MessageUpdate.UpdateType.RECEIVED) {
            if (!Constants.SERVICE_NAME.equals(metadata.getServiceName()) || metadata.getType() != PayloadType.CUSTOM) {
                log.info("Unsupported message type " + metadata.toString() + ", ignoring");
                return;
            }
            Jid jid = JidUtils.constructJid(sessionId, false);
            Conversation conversation = xmppConnectionService.find(CwebService.getAdapter().getAccount(), jid);
            if (sessionId.getType() == SessionType.SHARED_SESSION && conversation != null) {
                if (!CwebService.getAdapter().isActiveConference(conversation)) {
                    log.debug("Received message from closed conversation " + jid + ", ignoring");
                    return;
                }
            }
            byte[] data = TypedPayloadUtils.unwrapCustom(typedPayload);
            XmppConnection connection = CwebService.getAdapter().getAccount().getXmppConnection();
            connection.processCwebMessagesStream(sessionId, data);
        } else if (messageUpdate.type == MessageUpdate.UpdateType.ACK && metadata.getRefId() != null) {
            Jid jid = JidUtils.constructJid(sessionId, true);
            String messageUuid = Utils.asUuid(metadata.getRefId()).toString();
            if (jid != null && messageUuid != null) {
                Conversation conversation = xmppConnectionService.find(CwebService.getAdapter().getAccount(), jid);
                if (conversation != null) {
                    Message message = conversation.findSentMessageWithUuid(messageUuid);
                    if (message != null) {
                        xmppConnectionService.markMessage(message, Message.STATUS_SEND);
                    }
                }
            }
        }
    }

    public void onPhoneStateChange() {
        log.trace("onPhoneStateChange");
        schedulingProviderCallbacks.wakeUp();
    }
}
