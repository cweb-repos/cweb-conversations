package eu.siacs.conversations.cweb;

import org.cweb.schemas.storage.PublicStorageProfile;

public class CwebContact {
    private final byte[] id;
    private final PublicStorageProfile publicStorageProfile;

    public CwebContact(byte[] id, PublicStorageProfile publicStorageProfile) {
        this.id = id;
        this.publicStorageProfile = publicStorageProfile;
    }

    public byte[] getId() {
        return id;
    }

    public PublicStorageProfile getPublicStorageProfile() {
        return publicStorageProfile;
    }
}
