namespace java eu.siacs.conversations.cweb.schemas

struct LocalState {
  1: optional bool privateStorageProfileFromEmbedded
  2: optional binary id
  3: optional binary secretStorageKey
  4: optional binary storageProfileInviteAdminIdentityReference
}
