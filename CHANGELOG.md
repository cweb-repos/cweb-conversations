# Changelog

### Version 2.13.0

* Easier access to 'Show QR code'
* Support PEP Native Bookmarks
* Add support for SDP Offer / Answer Model (Used by SIP gateways)
* Raise target API to Android 14

### Version 2.12.12
* Pull upstream Conversations 2.12.12

### Version 2.12.8
* Pull upstream Conversations 2.12.8

### Version 2.10.10
* Switch to Conversations versions
* Pull upstream Conversations 2.10.10
* Fix account creation pages not saving state

### Version 0.1.6
* Pull upstream Conversations 2.10.5

### Version 0.1.5
* Initial release
* Pull upstream Conversations 2.9.10
* In-app storage sharing

