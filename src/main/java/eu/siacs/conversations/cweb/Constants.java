package eu.siacs.conversations.cweb;

public class Constants {
    static final String SERVICE_NAME = "Conversations";

    static final String FILE_LOCAL_SUFFIX_AVATAR_PROFILE = "-profilePicture";
    static final String FILE_LOCAL_SUFFIX_AVATAR_CONFERENCE = "-mucAvatar";

    public static final String CWEB_SCHEME = "cweb";
    private static final String ACCOUNT_PATH_PREFIX = "account.";
    static final String ACCOUNT_PREFIX = CWEB_SCHEME + ":" + ACCOUNT_PATH_PREFIX;
    private static final String CONTACT_PATH_PREFIX = "contact.";
    static final String CONTACT_PREFIX = CWEB_SCHEME + ":" + CONTACT_PATH_PREFIX;
    static final String URL_PREFIX_ATTACHMENT_FILE = "http://cweb/file.";

    static final String FILE_PROPERTY_TYPE_KEY = "type";
    static final String FILE_PROPERTY_TYPE_ATTACHMENT = "attachment";
    static final String FILE_PROPERTY_TYPE_AVATAR = "avatar";

    static final int FILE_PART_SIZE_BYTES = 100000;
    static final long UPLOADED_FILES_TTL = 1000L * 60 * 60 * 24 * 14;

    static final String SHARE_SESSION_PROPERTY_KEY_CONFERENCE_SUBJECT = "confSubject";
}
