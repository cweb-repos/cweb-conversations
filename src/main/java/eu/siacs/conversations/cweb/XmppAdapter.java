package eu.siacs.conversations.cweb;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import org.apache.commons.lang3.StringUtils;
import org.cweb.InstanceContainer;
import org.cweb.communication.SharedSessionService;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.utils.Constants;
import org.cweb.utils.ThriftTextUtils;
import org.cweb.utils.ThriftUtils;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import eu.siacs.conversations.cweb.schemas.LocalState;
import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.entities.Bookmark;
import eu.siacs.conversations.entities.Contact;
import eu.siacs.conversations.entities.Conversation;
import eu.siacs.conversations.entities.Message;
import eu.siacs.conversations.entities.MucOptions;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.ui.interfaces.OnAvatarPublication;
import eu.siacs.conversations.xml.Element;
import eu.siacs.conversations.xml.LocalizedContent;
import eu.siacs.conversations.xmpp.Jid;
import eu.siacs.conversations.xmpp.stanzas.AbstractStanza;

public class XmppAdapter {
    private final Logger log;
    private final CwebService cwebService;
    private final CwebMessages cwebMessages;
    private final InstanceContainer instanceContainer;
    private XmppConnectionService xmppConnectionService;
    private final AvatarHelper avatarHelper;

    private Account account;
    private final Map<ByteBuffer, PublicStorageProfile> newPublicStorageProfiles = new HashMap<>();
    private final LoadingCache<Jid, String> jid2nickname;

    public XmppAdapter(CwebService cwebService, CwebMessages cwebMessages, InstanceContainer instanceContainer, XmppConnectionService xmppConnectionService) {
        this.log = LoggerFactory.getLogger(XmppAdapter.class);
        this.cwebService = cwebService;
        this.cwebMessages = cwebMessages;
        this.instanceContainer = instanceContainer;
        this.xmppConnectionService = xmppConnectionService;
        this.avatarHelper = new AvatarHelper(instanceContainer, xmppConnectionService);
        this.jid2nickname = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(20, TimeUnit.HOURS)
                .build(new CacheLoader<Jid, String>() {
                    @Override
                    public String load(@NonNull Jid jid) throws Exception {
                        byte[] id = JidUtils.getCwebId(jid);
                        if (id == null) {
                            return null;
                        }
                        return getNickname(id);
                    }
                });
    }

    public void setXmppConnectionService(XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;
        avatarHelper.setXmppConnectionService(xmppConnectionService);
        account = null;
    }

    public PublicStorageProfile getNewPublicStorageProfileCached(byte[] id) {
        return newPublicStorageProfiles.get(ByteBuffer.wrap(id));
    }

    public boolean isInNewPublicStorageProfileCache(byte[] id) {
        return newPublicStorageProfiles.containsKey(ByteBuffer.wrap(id));
    }

    public void cacheNewStorageProfile(CwebContact cwebContact) {
        newPublicStorageProfiles.put(ByteBuffer.wrap(cwebContact.getId()), cwebContact.getPublicStorageProfile());
    }

    public Jid getOwnJid() {
        return getAccount().getJid();
    }

    public String createContactUri(Contact contact) {
        byte[] id = JidUtils.getCwebId(contact.getJid());
        PublicStorageProfile publicStorageProfile;
        if (Arrays.equals(id, instanceContainer.getOwnId())) {
            publicStorageProfile = instanceContainer.getIdentityService().getIdentityDescriptor().getStorageProfile();
        } else {
            publicStorageProfile = instanceContainer.getRemoteIdentityService().getPublicStorageProfile(id);
        }
        if (publicStorageProfile == null) {
            return null;
        }
        return eu.siacs.conversations.cweb.Constants.CONTACT_PREFIX + CwebService.idToString(id) + "." + ThriftTextUtils.toUrlSafeString(publicStorageProfile);
    }

    public static CwebContact parseCwebContactUri(String cwebContactUriStr) {
        String contactStr;
        int contactPrefixIdx = cwebContactUriStr.indexOf(eu.siacs.conversations.cweb.Constants.CONTACT_PREFIX);
        if (contactPrefixIdx == -1) {
            contactStr = cwebContactUriStr;
        } else {
            contactStr = cwebContactUriStr.substring(contactPrefixIdx + eu.siacs.conversations.cweb.Constants.CONTACT_PREFIX.length());
        }
        return parseCwebContact(contactStr);
    }

    private static CwebContact parseCwebContact(String cwebContactStr) {
        int separatorIdx = cwebContactStr.indexOf('.');
        if (separatorIdx <= 0) {
            return null;
        }
        String idStr = cwebContactStr.substring(0, separatorIdx);
        String publicStorageProfileStr = cwebContactStr.substring(separatorIdx + 1);
        return parseCwebContact(idStr, publicStorageProfileStr);
    }

    private static CwebContact parseCwebContact(String idStr, String publicStorageProfileStr) {
        byte[] id = CwebService.idFromString(idStr);
        if (id == null) {
            return null;
        }
        PublicStorageProfile publicStorageProfile = ThriftTextUtils.fromUrlSafeString(publicStorageProfileStr, PublicStorageProfile.class);
        if (publicStorageProfile == null) {
            return null;
        }
        return new CwebContact(id, publicStorageProfile);
    }

    Contact addContact(IdentityReference identityRef) {
        Contact contact = getContact(identityRef.getId());
        if (contact != null) {
            return contact;
        }
        instanceContainer.getRemoteIdentityService().setRemoteStorageProfile(identityRef);
        return addContact(identityRef.getId());
    }

    Contact getContact(byte[] id) {
        Jid jid = JidUtils.constructContactJid(id, false);
        List<Contact> contacts = xmppConnectionService.findContacts(jid, null);
        Contact contact = null;
        if (!contacts.isEmpty()) {
            contact = contacts.get(0);
        }
        return contact;
    }

    synchronized Contact addContact(byte[] id) {
        Contact contact = getContact(id);
        if (contact != null) {
            return contact;
        }
        Jid jid = JidUtils.constructContactJid(id, false);
        Account account = getAccount();
        contact = account.getRoster().getContact(jid);
        xmppConnectionService.createContact(contact, true);
        updateContactFromProfileOrIdentity(id);
        return contact;
    }

    public synchronized boolean onNewContactAdded(final Jid jid) {
        if (!JidUtils.isCwebContact(jid)) {
            return false;
        }
        final byte[] id = JidUtils.getCwebId(jid);
        final PublicStorageProfile publicStorageProfile = getNewPublicStorageProfileCached(id);
        if (publicStorageProfile == null && instanceContainer.getRemoteIdentityService().getPublicStorageProfile(id) == null) {
            log.error("Failed to find storage profile when adding " + org.cweb.utils.Utils.getDebugStringFromId(id));
            return false;
        }
        Threads.submitBackgroundTask(new Runnable() {
            @Override
            public void run() {
                if (publicStorageProfile != null) {
                    boolean success = instanceContainer.getRemoteIdentityService().setRemoteStorageProfile(id, publicStorageProfile);
                    if (!success) {
                        log.error("Failed to fetch identity for " + Utils.getDebugStringFromId(id));
                        return;
                    }
                }
                updateContactFromProfileOrIdentity(id);
                cwebMessages.addSession(new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(id)));
            }
        });
        return true;
    }

    private List<Property> getIdentityProperties(byte[] id) {
        List<Property> properties = null;
        IdentityProfile profile = instanceContainer.getIdentityProfileReadService().getProfile(id);
        if (profile != null) {
            properties = profile.getProperties();
        } else {
            LocalIdentityDescriptorState identityDescriptorState = instanceContainer.getRemoteIdentityService().get(id);
            if (identityDescriptorState != null) {
                properties = identityDescriptorState.getDescriptor().getOwnProperties();
            }
        }
        return properties;
    }

    public String getNickname(Jid jid) {
        return jid2nickname.getUnchecked(jid);
    }

    private String getNickname(byte[] id) {
        if (Arrays.equals(id, instanceContainer.getOwnId())) {
            return getAccount().getDisplayName();
        }

        List<Property> properties = getIdentityProperties(id);
        return getNickname(properties);
    }

    private String getNickname(List<Property> properties) {
        if (properties == null || properties.isEmpty()) {
            return null;
        }
        String nickname = CwebService.getNicknameProperty(properties);
        if (nickname == null || StringUtils.isBlank(nickname)) {
            return null;
        }
        return nickname;
    }

    synchronized void updateContactFromProfileOrIdentity(byte[] id) {
        List<Property> properties = getIdentityProperties(id);
        if (properties == null || properties.isEmpty()) {
            return;
        }

        Jid jid = JidUtils.constructContactJid(id, false);

        jid2nickname.invalidate(jid);

        List<Contact> contacts = xmppConnectionService.findContacts(jid, null);
        if (contacts.isEmpty()) {
            log.info("Contact not found " + Utils.getDebugStringFromId(id));
            return;
        }

        for (Contact contact : contacts) {
            boolean changed = false;

            String nickname = CwebService.getNicknameProperty(properties);
            if (nickname != null && !StringUtils.isBlank(nickname) && !nickname.equals(contact.getServerName())) {
                contact.setServerName(nickname);
                changed = true;
            }

            if (!Arrays.equals(id, instanceContainer.getOwnId())) {
                FileReference profilePictureFileReference = CwebService.getProfilePictureProperty(properties);
                if (profilePictureFileReference != null) {
                    avatarHelper.downloadAndUpdateContactAvatar(id, contact, profilePictureFileReference);
                }
            }

            if (changed) {
                xmppConnectionService.getAvatarService().clear(contact);
                xmppConnectionService.syncRoster(getAccount());
                xmppConnectionService.updateConversationUi();
                xmppConnectionService.updateRosterUi();
            }
        }
    }

    public boolean sendMessage(Account account, Message message, AbstractStanza packet, Jid jid) {
        if (!account.isCwebAccount()) {
            return false;
        }

        SessionId sessionId = JidUtils.getCwebSessionId(jid);
        if (sessionId == null) {
            return false;
        }

        if (sessionId.getType() == SessionType.COMM_SESSION) {
            List<Contact> contacts = xmppConnectionService.findContacts(jid, null);
            if (contacts.isEmpty()) {
                addContact(sessionId.getId());
            }
        }

        sendMessage(message, packet, sessionId);
        return true;
    }

    private void sendMessage(final Message message, AbstractStanza packet, final SessionId sessionId) {
        String dataStr = packet.toString();
        final byte[] data = dataStr.getBytes(Constants.CHARSET_UTF8);
        Threads.submitBackgroundTask(() -> sendMessageInternal(message, data, sessionId));
    }

    private void sendMessageInternal(Message message, byte[] data, SessionId sessionId) {
        byte[] refId = message != null && message.getUuid() != null ? eu.siacs.conversations.cweb.Utils.asBytes(UUID.fromString(message.getUuid())) : null;
        boolean success = cwebMessages.sendMessageInternal(data, sessionId, refId);

        if (message != null) {
            if (!success) {
                xmppConnectionService.markMessage(message, Message.STATUS_SEND_FAILED);
            }
        }
    }

    public void addAdminAsContact() {
        LocalState localState = cwebService.getLocalState();
        if (localState.isSetStorageProfileInviteAdminIdentityReference()) {
            IdentityReference adminIdentityRef = ThriftUtils.deserialize(localState.getStorageProfileInviteAdminIdentityReference(), IdentityReference.class);
            addContact(adminIdentityRef);
        }
    }

    public synchronized void refreshConversations(List<Conversation> conversations) {
        List<SessionId> sessionIds = new ArrayList<>(conversations.size());
        for (Conversation conversation : conversations) {
            Jid jid = conversation.getJid();
            SessionId sessionId = JidUtils.getCwebSessionId(jid);
            if (sessionId == null) {
                continue;
            }
            if (sessionId.getType() == SessionType.COMM_SESSION) {
                List<Contact> contacts = xmppConnectionService.findContacts(jid, null);
                if (contacts.isEmpty()) {
                    continue;
                }
                updateContactFromProfileOrIdentity(sessionId.getId());
            } else if (sessionId.getType() == SessionType.SHARED_SESSION) {
                if (!isActiveConference(conversation)) {
                    continue;
                }
                SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId.getId());
                if (metadata == null) {
                    deleteConference(conversation);
                    continue;
                }
                updateConferenceFromDescriptor(sessionId.getId(), metadata, conversation);
            }
            xmppConnectionService.databaseBackend.updateConversation(conversation);
            sessionIds.add(sessionId);
        }
        xmppConnectionService.syncRoster(getAccount());
        cwebMessages.setSessions(sessionIds);
    }

    private boolean deleteConference(Conversation conversation) {
        Account account = conversation.getAccount();
        xmppConnectionService.archiveConversation(conversation);
        Bookmark bookmark = findBookmark(conversation);
        if (bookmark == null) {
            return false;
        }
        bookmark.setConversation(null);
        account.getBookmarks().remove(bookmark);
        return true;
    }

    private Bookmark findBookmark(Conversation conversation) {
        Account account = conversation.getAccount();
        Jid conversationJid = conversation.getJid();
        for (Bookmark bookmark : account.getBookmarks()) {
            if (conversationJid.equals(bookmark.getJid())) {
                return bookmark;
            }
        }
        return null;
    }

    synchronized boolean isActiveConference(Conversation conversation) {
        // TODO: find out how to determine this
        return true;
    }

    public synchronized void addConference(Conversation conversation) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        if (sessionId == null) {
            log.warn("Invalid conference jid " + conversation.getJid());
            return;
        }
        SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
        if (metadata != null) {
            updateConferenceFromDescriptor(sessionId, metadata, conversation);
        } else {
            log.warn("Failed to fetch conference metadata for jid " + conversation.getJid());
            if (findBookmark(conversation) == null) {
                xmppConnectionService.saveConversationAsBookmark(conversation, null);
            }
        }
        cwebMessages.addSession(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId)));
        xmppConnectionService.updateConversationUi();
    }

    public synchronized void removeConference(Conversation conversation) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        if (sessionId == null) {
            log.warn("Invalid conference jid " + conversation.getJid());
            return;
        }
        cwebMessages.removeSession(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId)));
    }

    public boolean addConferenceParticipant(Conversation conversation, Jid jid) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        byte[] contactId = JidUtils.getCwebId(jid);
        return instanceContainer.getSharedSessionService().updateParticipants(sessionId, true, Collections.singletonList(contactId));
    }

    public void removeConferenceParticipant(Conversation conversation, Jid jid) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        byte[] contactId = JidUtils.getCwebId(jid);
        Threads.runAsNewThread(() -> instanceContainer.getSharedSessionService().updateParticipants(sessionId, false, Collections.singletonList(contactId)));
    }

    public Jid createSharedSession(String name) {
        byte[] sessionId = instanceContainer.getSharedSessionService().createNewSession(Collections.singletonList(CwebService.createNicknameProperty(name)));
        return JidUtils.constructConferenceJid(sessionId, false);
    }

    synchronized void updateConferenceFromDescriptor(byte[] sessionId, SharedSessionService.SessionMetadata metadata, Conversation conversation) {
        String name = getNickname(metadata.properties);
        conversation.setAttribute("muc_name", name);
        Bookmark bookmark = findBookmark(conversation);
        if (bookmark != null) {
            bookmark.setBookmarkName(name);
        } else {
            xmppConnectionService.saveConversationAsBookmark(conversation, name);
        }

        MucOptions mucOptions = conversation.getMucOptions();
        mucOptions.setOnline();

        String subject = CwebService.getSubjectProperty(metadata.properties);
        mucOptions.setSubject(subject);

        updateParticipants(metadata, mucOptions);
        // To reflect changed participants
        xmppConnectionService.getAvatarService().clear(conversation);

        if (!Arrays.equals(metadata.adminId, instanceContainer.getOwnId()) || conversation.getContact().getProfilePhoto() == null) {
            FileReference avatarFileReference = CwebService.getProfilePictureProperty(metadata.properties);
            if (avatarFileReference != null) {
                avatarHelper.downloadAndUpdateConferenceAvatar(sessionId, conversation, avatarFileReference);
            }
        }

        xmppConnectionService.updateRosterUi();
        xmppConnectionService.updateMucRosterUi();
    }

    private void updateParticipants(SharedSessionService.SessionMetadata metadata, MucOptions mucOptions) {
        Set<Jid> remainingRealJids = new HashSet<>();
        for (MucOptions.User user : mucOptions.getUsers()) {
            remainingRealJids.add(user.getRealJid());
        }

        for (byte[] participantId : metadata.participantIds) {
            boolean isSelf = Arrays.equals(participantId, instanceContainer.getOwnId());
            if (!isSelf && Arrays.equals(participantId, metadata.adminId)) {
                addContact(participantId);
            }

            Jid realJid = JidUtils.constructContactJid(participantId, true);
            if (realJid == null) {
                log.info("Invalid participant/nickname " + Utils.getDebugStringFromId(participantId));
                continue;
            }

            MucOptions.User user = isSelf ? mucOptions.getSelf() : mucOptions.findOrCreateUserByRealJid(realJid, realJid);
            if (user == null) {
                user = new MucOptions.User(mucOptions, realJid);
                user.setRealJid(realJid);
            }
            setRoleAndAffiliation(participantId, user, metadata);

            if (isSelf) {
                mucOptions.setSelf(user);
            } else {
                mucOptions.updateUser(user);
            }

            remainingRealJids.remove(realJid);
        }

        for (Jid realJid : remainingRealJids) {
            MucOptions.User user = mucOptions.findUserByRealJid(realJid);
            user.setRole(MucOptions.Role.NONE.toString());
            user.setAffiliation(MucOptions.Affiliation.OUTCAST.toString());
            mucOptions.updateUser(user);
        }
    }

    public void cwebUpdateReceived(Account account, SessionId sessionId, Element packet) {
        if (!account.isCwebAccount()) {
            return;
        }

        // Keep in sync with XmppConnection.sendStartStream
        packet.setAttribute("version", "1.0");
        packet.setAttribute("xml:lang", LocalizedContent.STREAM_LANGUAGE);
        packet.setAttribute("xmlns", "jabber:client");

        Jid fromJid = JidUtils.stringToJidSafe(packet.getAttribute("from"));
        Jid toJid = JidUtils.stringToJidSafe(packet.getAttribute("to"));
        Jid selfJid = xmppConnectionService.getAccounts().get(0).getJid();
        packet.setAttribute("to", selfJid.asBareJid().toString());
        if (sessionId.getType() == SessionType.COMM_SESSION && (fromJid == null || JidUtils.isCwebContact(fromJid))) {
            byte[] cwebId = sessionId.getId();
            if (fromJid != null && !Arrays.equals(cwebId, JidUtils.getCwebId(fromJid))) {
                log.debug("Invalid 'from' attr from " + Utils.getDebugStringFromId(cwebId) + " : " + fromJid);
            }
            if (toJid != null && !selfJid.equals(toJid)) {
                log.debug("Inconsistent 'to' attr " + Utils.getDebugStringFromId(cwebId) + " : " + toJid);
            }
            Jid jid = JidUtils.constructContactJid(cwebId, false);
            List<Contact> contacts = xmppConnectionService.findContacts(jid, null);
            Contact contact = !contacts.isEmpty() ? contacts.get(0) : null;
            if (contact == null) {
                log.debug("Received message from " + fromJid + ", adding to contacts");
                contact = addContact(cwebId);
            }
            packet.setAttribute("from", contact.getJid().asBareJid().toString());
        } else if (sessionId.getType() == SessionType.SHARED_SESSION) {
            packet.setAttribute("cwebMucFrom", fromJid.asBareJid().toString());
            byte[] sharedSessionId = sessionId.getId();
            Jid conferenceJid = JidUtils.constructConferenceJid(sharedSessionId, false);
            packet.setAttribute("from", conferenceJid.toString());
            Conversation conference = xmppConnectionService.find(account, conferenceJid);
            if (conference == null) {
                log.debug("Failed to find conference for " + toJid);
            }
        } else if (sessionId.getType() == SessionType.COMM_SESSION && JidUtils.isCwebSharedSession(fromJid)) {
            // Do nothing, conference invite
        } else {
            log.debug("Unsupported message " + sessionId.getType() + " from " + fromJid);
        }
    }

    public boolean amIanAdmin(Conversation conversation) {
        SharedSessionService.SessionMetadata sessionMetadata = getConferenceMetadata(conversation);
        if (sessionMetadata == null) {
            return false;
        }
        return Arrays.equals(instanceContainer.getOwnId(), sessionMetadata.adminId);
    }

    private SharedSessionService.SessionMetadata getConferenceMetadata(Conversation conversation) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        if (sessionId == null) {
            return null;
        }
        SharedSessionService.SessionMetadata sessionMetadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
        if (sessionMetadata == null) {
            return null;
        }
        return sessionMetadata;
    }

    public ArrayList<MucOptions.User> createConferenceUsers(MucOptions mucOptions, Conversation conversation) {
        ArrayList<MucOptions.User> result = new ArrayList<>();
        SharedSessionService.SessionMetadata sessionMetadata = getConferenceMetadata(conversation);
        if (sessionMetadata == null) {
            return result;
        }

        for (byte[] participantId : sessionMetadata.participantIds) {
            Jid jid = JidUtils.constructContactJid(participantId, true);
            if (jid == null || Arrays.equals(instanceContainer.getOwnId(), participantId)) {
                continue;
            }
            MucOptions.User user = new MucOptions.User(mucOptions, jid);
            user.setRealJid(jid);
            setRoleAndAffiliation(participantId, user, sessionMetadata);
            result.add(user);
        }

        return result;
    }

    private void setRoleAndAffiliation(byte[] userId, MucOptions.User user, SharedSessionService.SessionMetadata sessionMetadata) {
        boolean isAdmin = Arrays.equals(sessionMetadata.adminId, userId);
        boolean isSelf = Arrays.equals(userId, instanceContainer.getOwnId());
        if (isAdmin) {
            user.setRole(MucOptions.Role.MODERATOR.toString());
            user.setAffiliation(MucOptions.Affiliation.OWNER.toString());
        } else if (isSelf && sessionMetadata.isUnsubscribed) {
            user.setRole(MucOptions.Role.NONE.toString());
            user.setAffiliation(MucOptions.Affiliation.OUTCAST.toString());
        } else {
            user.setRole(MucOptions.Role.PARTICIPANT.toString());
            user.setAffiliation(MucOptions.Affiliation.MEMBER.toString());
        }
    }

    public MucOptions.User createConferenceUserSelf(MucOptions mucOptions, Conversation conversation) {
        Jid fullJid = getOwnJid();
        MucOptions.User user = new MucOptions.User(mucOptions, fullJid);
        user.setRealJid(fullJid);
        SharedSessionService.SessionMetadata sessionMetadata = getConferenceMetadata(conversation);
        if (sessionMetadata == null) {
            return user;
        }
        setRoleAndAffiliation(instanceContainer.getOwnId(), user, sessionMetadata);
        return user;
    }

    void updateSelfRoleInConference(byte[] sessionId) {
        Jid conferenceJid = JidUtils.constructConferenceJid(sessionId, false);
        Conversation conversation = xmppConnectionService.find(getAccount(), conferenceJid);
        if (conversation == null) {
            return;
        }
        SharedSessionService.SessionMetadata sessionMetadata = getConferenceMetadata(conversation);
        if (sessionMetadata == null) {
            return;
        }
        setRoleAndAffiliation(instanceContainer.getOwnId(), conversation.getMucOptions().getSelf(), sessionMetadata);
    }

    public static Account findCwebAccount(XmppConnectionService xmppConnectionService) {
        List<Account> accounts = xmppConnectionService.getAccounts();
        if (accounts.isEmpty()) {
            return null;
        }
        Preconditions.checkArgument(accounts.size() == 1);
        Account account = accounts.get(0);
        Preconditions.checkArgument(account.isCwebAccount());
        return account;
    }

    public Account getAccount() {
        if (account != null) {
            return account;
        }
        if (xmppConnectionService == null) {
            return null;
        }
        account = XmppAdapter.findCwebAccount(xmppConnectionService);
        return account;
    }

    public static void shareMyCwebContact(Context context, Account account) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Add " + account.getDisplayName() + " to your contacts");
        intent.putExtra(Intent.EXTRA_TEXT, CwebService.getAdapter().createContactUri(account.getSelfContact()));
        context.startActivity(Intent.createChooser(intent, "Share Cweb id with..."));
    }

    public synchronized void updateConferenceName(Jid jid, String name) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(jid);
        Property property = CwebService.createNicknameProperty(name);
        instanceContainer.getSharedSessionService().updateProperties(sessionId, Collections.singletonList(property));
    }

    public synchronized void updateConferenceSubject(Jid jid, String name) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(jid);
        Property property = CwebService.createSubjectProperty(name);
        instanceContainer.getSharedSessionService().updateProperties(sessionId, Collections.singletonList(property));
    }

    public void publishProfileAvatar(Uri avatarUri, OnAvatarPublication callback) {
        avatarHelper.publishProfileAvatar(avatarUri, callback);
    }

    public void publishConferenceAvatar(Conversation conversation, Uri avatarUri, OnAvatarPublication callback) {
        avatarHelper.publishConferenceAvatar(conversation, avatarUri, callback);
    }
}
