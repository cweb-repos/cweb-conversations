package eu.siacs.conversations.cweb;

import eu.siacs.conversations.cweb.schemas.LocalState;

public interface LocalStateUpdater {
    boolean update(LocalState state);
}
