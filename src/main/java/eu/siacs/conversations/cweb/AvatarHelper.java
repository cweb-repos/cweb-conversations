package eu.siacs.conversations.cweb;

import android.net.Uri;

import org.apache.commons.lang3.tuple.Triple;
import org.cweb.InstanceContainer;
import org.cweb.communication.SharedSessionService;
import org.cweb.files.FileDownloadService;
import org.cweb.files.FileUploadService;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.siacs.conversations.R;
import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.entities.Contact;
import eu.siacs.conversations.entities.Conversation;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.ui.interfaces.OnAvatarPublication;

public class AvatarHelper {
    private final Logger log;
    private final InstanceContainer instanceContainer;
    private XmppConnectionService xmppConnectionService;

    public AvatarHelper(InstanceContainer instanceContainer, XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;
        this.log = LoggerFactory.getLogger(AvatarHelper.class);
        this.instanceContainer = instanceContainer;
    }

    public void setXmppConnectionService(XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;
    }

    private static String getExtensionFromUri(Uri uri) {
        String filename = uri.getPath();
        int pos = filename == null ? -1 : filename.lastIndexOf('.');
        return pos > 0 ? filename.substring(pos + 1) : null;
    }

    private String getAvatarFileName(byte[] sessionId, String idSuffix, Uri imageUri) {
        String extension = getExtensionFromUri(imageUri);
        return Utils.toBase32String(sessionId) + idSuffix + (extension != null ? "." + extension : "");
    }

    private LocalUploadedFileInfo publishAvatar(byte[] sessionId, String idSuffix, Uri imageUri, FileReference currentAvatarFileReference) {
        String filename = getAvatarFileName(sessionId, idSuffix, imageUri);
        List<Property> properties = Collections.singletonList(new Property(Constants.FILE_PROPERTY_TYPE_KEY, PropertyValue.str(Constants.FILE_PROPERTY_TYPE_AVATAR)));
        Triple<LocalUploadedFileInfo, FileMetadata, FileUploadService.UploadError> uploadResult = instanceContainer.getFileUploadService().upload(imageUri.getPath(), filename, properties, Constants.FILE_PART_SIZE_BYTES, true);
        FileUploadService.UploadError error = uploadResult.getRight();
        if (error != null) {
            return null;
        }

        if (currentAvatarFileReference != null) {
            instanceContainer.getFileUploadService().delete(currentAvatarFileReference.getFileId(), false);
        }

        return uploadResult.getLeft();
    }

    public void publishProfileAvatar(Uri image, OnAvatarPublication callback) {
        FileReference currentAvatarFileReference = CwebService.getProfilePictureProperty(instanceContainer.getIdentityProfilePostService().getProfile().getProperties());
        LocalUploadedFileInfo fileInfo = publishAvatar(instanceContainer.getOwnId(), Constants.FILE_LOCAL_SUFFIX_AVATAR_PROFILE, image, currentAvatarFileReference);
        if (fileInfo == null) {
            callback.onAvatarPublicationFailed(R.string.error_publish_avatar_offline);
            return;
        }

        Property profilePictureProperty = CwebService.createProfilePictureProperty(fileInfo.getFileReference());
        instanceContainer.getIdentityProfilePostService().updateProperties(Collections.singletonList(profilePictureProperty));
        callback.onAvatarPublicationSucceeded();
    }

    void downloadAndUpdateContactAvatar(byte[] id, Contact contact, FileReference profilePictureFileReference) {
        downloadAndUpdateAvatar(id, contact.getProfilePhoto(), profilePictureFileReference, uri -> {
            contact.setPhotoUri(uri.toString());
            xmppConnectionService.syncRoster(contact.getAccount());
            xmppConnectionService.getAvatarService().clear(contact);
            xmppConnectionService.updateConversationUi();
            xmppConnectionService.updateRosterUi();
            xmppConnectionService.updateMucRosterUi();
        });
    }

    public void publishConferenceAvatar(Conversation conversation, Uri image, OnAvatarPublication callback) {
        byte[] sessionId = JidUtils.getCwebSharedSessionId(conversation.getJid());
        SharedSessionService.SessionMetadata sessionMetadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
        if (!Arrays.equals(sessionMetadata.adminId, instanceContainer.getOwnId())) {
            callback.onAvatarPublicationFailed(R.string.error_publish_avatar_server_reject);
            return;
        }

        FileReference currentAvatarFileReference = CwebService.getProfilePictureProperty(sessionMetadata.properties);
        LocalUploadedFileInfo fileInfo = publishAvatar(sessionId, Constants.FILE_LOCAL_SUFFIX_AVATAR_CONFERENCE, image, currentAvatarFileReference);
        if (fileInfo == null) {
            callback.onAvatarPublicationFailed(R.string.error_publish_avatar_offline);
            return;
        }

        Property profilePictureProperty = CwebService.createProfilePictureProperty(fileInfo.getFileReference());
        instanceContainer.getSharedSessionService().updateProperties(sessionId, Collections.singletonList(profilePictureProperty));
        updateConferenceAvatar(conversation, image);
        callback.onAvatarPublicationSucceeded();
    }

    private void updateConferenceAvatar(Conversation conversation, Uri uri) {
        Account account = conversation.getAccount();
        Contact contact = conversation.getContact();
        contact.setPhotoUri(uri.toString());
        xmppConnectionService.databaseBackend.updateConversation(conversation);
        xmppConnectionService.syncRoster(account);
        xmppConnectionService.getAvatarService().clear(conversation);
        xmppConnectionService.getAvatarService().clear(contact);
        xmppConnectionService.updateConversationUi();
        xmppConnectionService.updateMucRosterUi();
    }

    void downloadAndUpdateConferenceAvatar(byte[] sessionId, Conversation conversation, FileReference avatarFileReference) {
        downloadAndUpdateAvatar(sessionId, conversation.getContact().getProfilePhoto(), avatarFileReference, uri -> updateConferenceAvatar(conversation, uri));
    }

    interface OnAvatarDownloaded {
        void onDownloadSuccess(Uri uri);
    }

    private void downloadAndUpdateAvatar(byte[] id, String currentAvatarUri, FileReference avatarFileReference, OnAvatarDownloaded callback) {
        byte[] fileId = avatarFileReference.getFileId();
        String uriFilePrefix = Utils.toBase32String(id) + "_" + Utils.toBase32String(fileId);
        if (currentAvatarUri != null && currentAvatarUri.contains(uriFilePrefix) && (new File(Uri.parse(currentAvatarUri).getPath())).isFile()) {
            return;
        }
        Threads.runAsNewThread(() -> {
            Uri uri = xmppConnectionService.getFileBackend().getAvatarUri(uriFilePrefix);
            boolean downloadSuccess = downloadAvatarFile(avatarFileReference, uri);
            if (!downloadSuccess) {
                return;
            }

            if (currentAvatarUri != null) {
                String filename = Uri.parse(currentAvatarUri).getPath();
                boolean deleted = new File(filename).delete();
                if (!deleted) {
                    log.debug("Failed to delete avatar file " + currentAvatarUri);
                }
            }
            callback.onDownloadSuccess(uri);
        });
    }

    private boolean downloadAvatarFile(FileReference fileReference, Uri uri) {
        instanceContainer.getFileDownloadService().addSharedFileReference(fileReference);
        org.apache.commons.lang3.tuple.Pair<FileMetadata, FileDownloadService.DownloadError> downloadResult = instanceContainer.getFileDownloadService().startDownload(fileReference, uri.getPath());
        if (downloadResult.getRight() != null) {
            log.debug("Failed to start avatar download: " + downloadResult.getRight());
            return false;
        }

        FileDownloadService.DownloadState downloadState;
        while (true) {
            downloadState = instanceContainer.getFileDownloadService().getDownloadState(fileReference);
            if (downloadState == null || downloadState.completedAt != null || downloadState.abortedAt != null) {
                break;
            }
            org.cweb.utils.Threads.sleepChecked(2000);
        }

        if (downloadState == null || downloadState.completedAt == null) {
            return false;
        }
        return true;
    }
}
